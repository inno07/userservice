package routes

import (
	"github.com/gofiber/fiber/v2"
	"userservice/controllers"
)

func Setup(app *fiber.App) {
	app.Post("/api/users/signup", controllers.Signup)
	app.Post("/api/users/login", controllers.Login)
}
