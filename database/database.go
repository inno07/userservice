package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"userservice/models"
)

var DB *gorm.DB

func Connect() {
	dsn := "host=postgres user=admin password=password dbname=users port=5432 sslmode=disable"

	connection, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Could not connect to database.")
	}

	DB = connection

	connection.AutoMigrate(&models.User{})
}
